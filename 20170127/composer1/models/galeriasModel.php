<?php 

require_once('models/galeriaModel.php');

//include('models/ClienteModel.php');
//Modelo Galerias
Class Galerias
{
protected $conexion;
protected $elementos;
protected $tabla;

		public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
		$this->tabla='imagenes';
		
	}

	public function dimeImagen($id)
	{
			
			$sql="SELECT * FROM $this->tabla WHERE idImagen=$id";
			$consulta=$this->conexion->query($sql);
			$fila=$consulta->fetch_array();
			$gal=new galeria($fila['idImagen'], $fila['tituloImagenes'], $fila['ficheroImagenes']);
			return $gal;
		
	}

	public function listado()
	{
			
		// 	$sql="SELECT * FROM $tabla LIMIT 0,1";
		// $consulta=$this->conexion->query($sql);
		// $this->campoOrden=$consulta->fetch_fields()[0]->name;


			$sql="SELECT * FROM $this->tabla";
			$consulta=$this->conexion->query($sql);
			while($fila=$consulta->fetch_array())
			{
				$gal=new galeriaModel($fila['idImagen'], $fila['tituloImagen'], $fila['ficheroImagen']);
				$this->elementos[]=$gal;
			}
			return $this->elementos;
		
	}

}
 ?>