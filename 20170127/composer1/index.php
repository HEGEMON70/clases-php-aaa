<?php 
require __DIR__ . '/vendor/autoload.php'; //enlace de composer
//cargamos BBDD
require_once('config.php');
require_once('includes/conexion.php');


//establezco el directorio base de mis archivos de plantilla
$loader = new Twig_Loader_Filesystem('views');
//Me creo el objeto de la clase Twig_Enviroment
$twig = new Twig_Environment($loader, array());//el segundo argumento es un array vacio(para parametros)

//le digo a mi objeto twig, que plantillas cargar
$template = $twig->loadTemplate('base.twig.html');

$p=isset($_GET['p'])?isset($_GET['p']):'galeria.php';
// if(isset($_GET['p'])){
// 	$p=$_GET['p'];
// }else{
// 	$p='usuarios.php';
// }
//llamo al controlador
require('controllers/'.$p);


 ?>

