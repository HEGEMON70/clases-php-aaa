USE
  empresa;

SELECT * FROM( productos
  LEFT JOIN
    categorias ON categorias.idCategoria = productos.idCategoria
  )
LEFT JOIN
  marcas ON marcas.idMarca = productos.idMarca;