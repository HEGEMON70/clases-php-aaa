-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2017 a las 19:55:56
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `empresa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
`idCategoria` int(11) NOT NULL,
  `nombreCategoria` varchar(150) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `nombreCategoria`) VALUES
(1, 'antibiotico'),
(2, 'desinfectante'),
(3, 'analgesico'),
(4, 'antiviral');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
`idCli` int(11) NOT NULL,
  `nombreCli` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `correoCli` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaAltaCli` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idCli`, `nombreCli`, `correoCli`, `fechaAltaCli`) VALUES
(1, 'Helen', 'hbutler0@miibeian.gov.cn', '2016-05-20 06:29:40'),
(2, 'Ruby', 'rroberts1@smh.com.au', '2016-04-05 13:05:36'),
(3, 'Kenneth', 'kfuller2@hud.gov', '2016-06-28 20:17:44'),
(4, 'Billy', 'bstevens3@google.fr', '2016-01-16 04:03:00'),
(5, 'Deborah', 'dmorris4@upenn.edu', '2016-08-03 05:19:22'),
(6, 'Steven', 'swashington5@msn.com', '2016-10-04 23:44:05'),
(7, 'Michael', 'mlarson6@disqus.com', '2016-07-20 00:52:11'),
(8, 'Tina', 'tweaver7@reddit.com', '2016-10-20 17:04:07'),
(9, 'Victor', 'vbell8@dyndns.org', '2016-03-04 18:34:44'),
(10, 'Sharon', 'smoore9@arizona.edu', '2016-07-28 12:55:35'),
(11, 'Walter', 'whunta@w3.org', '2016-05-20 14:37:48'),
(12, 'Carolyn', 'cturnerb@squarespace.com', '2016-09-12 10:48:52'),
(13, 'Albert', 'ahunterc@ca.gov', '2016-04-12 11:31:46'),
(14, 'Lawrence', 'lfieldsd@edublogs.org', '2016-05-28 20:03:47'),
(15, 'Jane', 'jjacksone@umich.edu', '2016-05-15 04:04:39'),
(16, 'Steve', 'smoralesf@businesswire.com', '2016-03-08 16:19:38'),
(17, 'Jeremy', 'jrobinsong@themeforest.net', '2016-02-19 10:24:46'),
(18, 'Howard', 'hbowmanh@patch.com', '2016-01-08 17:38:48'),
(19, 'Ronald', 'rdavisi@networkadvertising.org', '2015-12-23 04:14:27'),
(20, 'Bobby', 'bspencerj@hud.gov', '2016-07-16 22:35:10'),
(21, 'Annie', 'athompsonk@discovery.com', '2016-10-29 01:08:13'),
(22, 'Melissa', 'mgutierrezl@loc.gov', '2016-04-19 14:28:32'),
(23, 'Albert', 'acolem@wordpress.com', '2016-10-14 01:26:10'),
(24, 'Marie', 'mfranklinn@whitehouse.gov', '2016-08-02 05:42:57'),
(25, 'Carol', 'chowello@arstechnica.com', '2016-09-06 21:02:59'),
(26, 'George', 'gchapmanp@cmu.edu', '2016-11-18 00:44:56'),
(27, 'Adam', 'amorrisonq@time.com', '2015-12-25 11:36:44'),
(28, 'Janet', 'jlewisr@bing.com', '2016-11-28 02:09:40'),
(29, 'Jessica', 'jwashingtons@rakuten.co.jp', '2016-05-10 14:47:37'),
(30, 'Willie', 'wtuckert@google.pl', '2016-04-22 20:03:42'),
(31, 'Denise', 'dhendersonu@oracle.com', '2016-06-20 20:17:20'),
(32, 'Frances', 'fhendersonv@homestead.com', '2016-08-01 12:47:12'),
(33, 'Teresa', 'tfisherw@mayoclinic.com', '2016-01-05 11:01:43'),
(34, 'Eric', 'ewilliamsonx@jalbum.net', '2016-05-10 02:30:50'),
(35, 'Chris', 'charpery@springer.com', '2016-04-20 14:46:44'),
(36, 'William', 'wblackz@google.de', '2016-10-11 06:04:45'),
(37, 'Jean', 'jgordon10@imdb.com', '2016-01-27 09:04:06'),
(38, 'Mary', 'mroberts11@gizmodo.com', '2016-07-13 22:47:46'),
(39, 'Jean', 'jbaker12@seesaa.net', '2016-04-29 06:21:07'),
(40, 'Philip', 'pgutierrez13@sphinn.com', '2016-11-28 09:37:18'),
(41, 'Bruce', 'bjohnston14@miitbeian.gov.cn', '2016-04-24 01:37:04'),
(42, 'Matthew', 'mryan15@opera.com', '2016-07-10 00:37:10'),
(43, 'Ruth', 'rfrazier16@businesswire.com', '2016-07-31 15:35:14'),
(44, 'Ralph', 'rcarroll17@php.net', '2016-04-24 23:32:29'),
(45, 'Linda', 'lwells18@sciencedirect.com', '2016-08-24 21:17:29'),
(46, 'Jimmy', 'jwood19@tumblr.com', '2016-11-16 17:02:50'),
(47, 'Joseph', 'jgibson1a@fc2.com', '2016-10-21 20:25:37'),
(48, 'Jose', 'jwells1b@flavors.me', '2016-02-12 19:06:43'),
(49, 'Deborah', 'dwilliamson1c@cloudflare.com', '2016-07-23 17:53:09'),
(50, 'Willie', 'wbrown1d@mac.com', '2016-06-03 22:16:29'),
(51, 'Louise', 'lfox1e@diigo.com', '2016-03-17 15:22:22'),
(52, 'Shawn', 'swelch1f@webnode.com', '2016-01-05 11:35:06'),
(53, 'Roger', 'rray1g@indiatimes.com', '2016-01-27 20:07:30'),
(54, 'Dennis', 'dcarr1h@loc.gov', '2016-10-22 18:38:43'),
(55, 'Jessica', 'jwarren1i@technorati.com', '2016-05-25 18:56:26'),
(56, 'Henry', 'hcarroll1j@ebay.com', '2016-09-20 14:53:26'),
(57, 'Brandon', 'bgarrett1k@fotki.com', '2016-09-04 21:44:10'),
(58, 'Nancy', 'nmoreno1l@sourceforge.net', '2016-06-12 23:34:33'),
(59, 'Sharon', 'slopez1m@usatoday.com', '2016-02-09 23:06:18'),
(60, 'Douglas', 'dmoore1n@nhs.uk', '2016-03-17 11:49:53'),
(61, 'Gerald', 'galexander1o@barnesandnoble.com', '2016-07-19 02:38:07'),
(62, 'Angela', 'amitchell1p@angelfire.com', '2016-10-12 21:36:29'),
(63, 'Rose', 'redwards1q@imgur.com', '2016-06-05 02:10:04'),
(64, 'Ronald', 'rford1r@indiatimes.com', '2016-07-07 00:35:59'),
(65, 'Helen', 'hward1s@springer.com', '2016-06-29 04:23:11'),
(66, 'Nicole', 'nhunter1t@nytimes.com', '2016-02-27 16:58:33'),
(67, 'Michael', 'mhayes1u@buzzfeed.com', '2016-10-13 09:35:09'),
(68, 'Phillip', 'pduncan1v@g.co', '2016-06-10 09:24:28'),
(69, 'Marie', 'mwebb1w@google.it', '2016-12-12 19:56:57'),
(70, 'Carl', 'cmontgomery1x@dyndns.org', '2016-01-23 14:30:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE IF NOT EXISTS `imagenes` (
`idImagen` int(11) NOT NULL,
  `tituloImagen` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `ficheroImagen` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`idImagen`, `tituloImagen`, `ficheroImagen`) VALUES
(1, 'Paisaje muy bonito', 'tulipanes.jpg'),
(2, 'Paisaje desertico', 'desierto.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE IF NOT EXISTS `marcas` (
`idMarca` int(11) NOT NULL,
  `nombreMarca` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`idMarca`, `nombreMarca`) VALUES
(1, 'Lambon'),
(2, 'Reflex'),
(3, 'Bayer'),
(4, 'Generico'),
(5, 'Pheifer'),
(6, 'Abello'),
(7, 'Novordisk');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
`idNot` int(11) NOT NULL,
  `tituloNot` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `contenidoNot` longtext COLLATE utf8_spanish2_ci NOT NULL,
  `autorNot` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaNot` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idNot`, `tituloNot`, `contenidoNot`, `autorNot`, `fechaNot`) VALUES
(1, 'titulo 1', 'contenido 1', 'autor 1', '2016-12-15 00:00:00'),
(2, '22222', '22', '222', '2016-12-15 00:10:00'),
(3, 'primis in', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 'Lisa', '2016-05-31 05:54:35'),
(4, 'nam nulla', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 'Billy', '2016-06-21 15:11:07'),
(5, 'nulla neque', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'Lawrence', '2016-09-04 00:36:20'),
(6, 'in leo maecenas', 'Sed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 'Frances', '2016-01-30 20:39:41'),
(7, 'congue elementum', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'Wanda', '2016-01-30 16:10:25'),
(8, 'lorem ipsum dolor', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Edward', '2016-10-04 09:55:39'),
(9, 'pellentesque viverra pede', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'Terry', '2016-02-29 20:20:51'),
(10, 'vitae consectetuer eget', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Carlos', '2016-11-01 06:29:56'),
(11, 'turpis enim blandit', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.\n\nAenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'Kimberly', '2016-11-07 13:18:17'),
(12, 'luctus et ultrices', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'David', '2016-01-27 20:25:25'),
(13, 'ullamcorper augue a', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 'Gary', '2016-04-23 00:33:01'),
(14, 'viverra diam vitae', 'Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\n\nProin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 'Jean', '2016-09-21 04:52:15'),
(15, 'morbi non', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.\n\nVestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.\n\nDuis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 'George', '2016-01-13 21:41:27'),
(16, 'et commodo', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 'Christine', '2016-10-24 21:45:56'),
(17, 'vel est donec', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 'Jessica', '2016-10-21 08:46:52'),
(18, 'curabitur at ipsum', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'Ruth', '2016-03-23 23:44:40'),
(19, 'montes nascetur ridiculus', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'Kimberly', '2016-01-20 03:16:48'),
(20, 'enim leo rhoncus', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'Wanda', '2016-04-11 13:05:07'),
(21, 'donec diam neque', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'Evelyn', '2016-02-24 22:48:27'),
(22, 'aliquam non', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\n\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.', 'Eugene', '2016-03-12 20:53:53'),
(23, 'libero ut', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.\n\nIn sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 'Ronald', '2016-06-30 00:28:27'),
(24, 'sed nisl', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'Janet', '2016-10-03 06:38:10'),
(25, 'phasellus sit', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'Lillian', '2016-02-09 22:39:57'),
(26, 'in eleifend quam', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 'Denise', '2015-12-13 22:19:58'),
(27, 'eros suspendisse accumsan', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 'Stephen', '2016-07-16 19:11:35'),
(28, 'morbi vestibulum velit', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.\n\nPellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 'Raymond', '2016-03-30 18:11:29'),
(29, 'ipsum dolor sit', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 'Laura', '2016-05-14 05:24:05'),
(30, 'luctus et ultrices', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'Louise', '2016-02-25 23:37:00'),
(31, 'congue diam id', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'Sandra', '2016-01-29 03:49:28'),
(32, 'mus etiam', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'Brandon', '2016-07-14 20:21:21'),
(33, 'molestie lorem', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.\n\nDuis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'Brenda', '2016-08-12 13:15:48'),
(34, 'sed nisl', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Philip', '2016-01-13 03:48:30'),
(35, 'amet nulla', 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 'Teresa', '2016-09-29 00:01:20'),
(36, 'nisi eu orci', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 'Thomas', '2016-03-30 15:33:25'),
(37, 'dui proin leo', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.\n\nAliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 'Jeremy', '2016-02-06 06:16:17'),
(38, 'etiam vel', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Paula', '2016-06-02 16:37:49'),
(39, 'nonummy maecenas', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'Billy', '2016-05-10 09:30:44'),
(40, 'massa tempor convallis', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'Shawn', '2015-12-25 16:29:56'),
(41, 'justo nec', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.', 'Terry', '2016-08-30 10:08:14'),
(42, 'justo eu', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.\n\nPraesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 'Sean', '2016-04-12 13:32:32'),
(43, 'id lobortis', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 'Gary', '2015-12-20 00:33:57'),
(44, 'lobortis sapien', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.\n\nMaecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 'Rose', '2016-03-04 11:00:06'),
(45, 'orci vehicula condimentum', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.\n\nMaecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 'Susan', '2016-09-21 14:38:56'),
(46, 'mauris eget', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 'Nicole', '2016-10-07 17:12:51'),
(47, 'est risus', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'Anthony', '2016-09-05 14:26:05'),
(48, 'curabitur gravida', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.\n\nQuisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 'Beverly', '2016-01-22 23:37:18'),
(49, 'hac habitasse platea', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 'Margaret', '2016-09-03 04:22:35'),
(50, 'pretium quis lectus', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 'Betty', '2016-09-03 13:53:32'),
(51, 'nullam sit', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.\n\nIn hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Juan', '2016-01-01 08:49:43'),
(52, 'platea dictumst', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.\n\nCurabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', 'Carl', '2016-10-22 20:56:02'),
(53, 'consequat in', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Maria', '2016-09-24 12:31:17'),
(54, 'viverra dapibus', 'Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'Virginia', '2016-10-07 23:07:19'),
(55, 'mi sit amet', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 'Antonio', '2016-01-20 07:33:03'),
(56, 'justo pellentesque viverra', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 'Elizabeth', '2016-04-07 16:47:18'),
(57, 'a pede', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 'Betty', '2016-05-13 04:00:47'),
(58, 'vel enim', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.\n\nFusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.\n\nSed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 'Alan', '2016-07-27 03:02:47'),
(59, 'est et tempus', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.\n\nInteger ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 'Jonathan', '2016-01-27 09:40:35'),
(60, 'viverra diam vitae', 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Gregory', '2016-02-15 12:33:08'),
(61, 'consectetuer adipiscing elit', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'Donald', '2016-07-26 01:16:40'),
(62, 'quam a odio', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', 'Roy', '2016-11-09 19:37:57'),
(63, 'lobortis vel dapibus', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.\n\nCras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 'Billy', '2016-04-04 03:30:43'),
(64, 'lacus at', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 'Philip', '2016-08-23 13:59:36'),
(65, 'auctor gravida', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 'Martin', '2016-05-17 19:37:24'),
(66, 'ante ipsum', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.\n\nIn quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.\n\nMaecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 'Lillian', '2016-03-22 20:38:21'),
(67, 'nullam orci pede', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 'Lisa', '2016-07-17 22:19:40'),
(68, 'nunc nisl duis', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Ruby', '2016-07-14 10:44:52'),
(69, 'lobortis ligula', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 'Joan', '2016-07-30 17:11:41'),
(70, 'nulla quisque arcu', 'Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.\n\nNam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Maria', '2016-01-08 06:58:10'),
(71, 'at vulputate', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.\n\nInteger tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.\n\nPraesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 'Bobby', '2016-08-31 01:00:08'),
(72, 'sit amet erat', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.\n\nCurabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.\n\nPhasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 'Clarence', '2016-07-12 22:23:57'),
(73, 'varius integer', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'Rebecca', '2016-08-12 19:18:32'),
(74, 'non lectus', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'George', '2016-04-02 18:06:49'),
(75, 'ante ipsum primis', 'Fusce consequat. Nulla nisl. Nunc nisl.\n\nDuis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.\n\nIn hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 'Charles', '2016-04-24 21:33:57'),
(76, 'nascetur ridiculus mus', 'In congue. Etiam justo. Etiam pretium iaculis justo.', 'Philip', '2016-06-01 07:14:08'),
(77, 'sem fusce consequat', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\n\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.', 'Carlos', '2016-10-31 20:55:40'),
(78, 'libero non mattis', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 'Douglas', '2016-02-02 00:04:00'),
(79, 'platea dictumst etiam', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.\n\nDuis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 'Karen', '2015-12-21 16:23:49'),
(80, 'lacus at', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', 'Margaret', '2016-05-07 17:49:14'),
(81, 'pellentesque ultrices', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 'Aaron', '2015-12-25 03:49:19'),
(82, 'diam in magna', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 'Julie', '2016-01-21 11:56:27'),
(83, 'in sagittis', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 'Tammy', '2016-07-13 09:23:01'),
(84, 'ultricies eu', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.\n\nAenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 'Sarah', '2016-09-21 14:51:26'),
(85, 'platea dictumst', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\n\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 'Ryan', '2016-11-14 13:27:54'),
(86, 'nisl duis ac', 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 'Carl', '2016-09-16 23:14:26'),
(87, 'augue luctus tincidunt', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.\n\nQuisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 'Ruby', '2016-09-17 18:15:48'),
(88, 'lectus aliquam sit', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.\n\nMorbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.\n\nFusce consequat. Nulla nisl. Nunc nisl.', 'Jesse', '2016-12-12 00:27:33'),
(89, 'platea dictumst aliquam', 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.\n\nNullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 'Gloria', '2016-10-17 22:34:41'),
(90, 'orci vehicula condimentum', 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.\n\nNulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.\n\nCras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'Kathleen', '2016-07-04 22:18:55'),
(91, 'quisque erat', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.\n\nMauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', 'Ralph', '2016-04-21 16:46:21'),
(92, 'luctus et ultrices', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.\n\nCurabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 'Anthony', '2016-08-28 21:07:34'),
(93, 'ante vestibulum ante', 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'William', '2016-08-25 23:44:34'),
(94, 'suspendisse potenti in', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.\n\nDonec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', 'Diana', '2016-07-16 11:19:44'),
(95, 'quis orci eget', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.\n\nVestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 'Frances', '2016-08-27 19:34:27'),
(96, 'cum sociis natoque', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.\n\nCum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Dorothy', '2016-12-04 04:42:15'),
(97, 'convallis nunc proin', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 'Terry', '2016-11-18 23:43:05'),
(98, 'eget congue eget', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 'Harry', '2016-01-08 07:27:50'),
(99, 'elementum ligula', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 'Sharon', '2016-10-15 10:14:50'),
(100, 'a libero nam', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n\nEtiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Sharon', '2016-05-29 20:15:27'),
(101, 'erat fermentum', 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.\n\nIn congue. Etiam justo. Etiam pretium iaculis justo.', 'Jeremy', '2016-04-14 09:14:55'),
(102, 'vulputate justo', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 'Tina', '2016-07-28 10:12:34'),
(103, 'aaaa', 'aaaa', 'aaaa', '2016-12-23 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posologias`
--

CREATE TABLE IF NOT EXISTS `posologias` (
`idPosologia` int(11) NOT NULL,
  `nombrePosologia` varchar(100) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `posologias`
--

INSERT INTO `posologias` (`idPosologia`, `nombrePosologia`) VALUES
(1, 'Pastillas'),
(2, 'Sobres'),
(3, 'Jarabe'),
(4, 'Inyectable'),
(5, 'Supositorio'),
(6, 'Crema');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posologiasproductos`
--

CREATE TABLE IF NOT EXISTS `posologiasproductos` (
  `idPosologia` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `posologiasproductos`
--

INSERT INTO `posologiasproductos` (`idPosologia`, `idProducto`) VALUES
(1, 110),
(2, 105),
(2, 108),
(3, 105),
(4, 108),
(5, 108),
(109, 3),
(109, 4),
(109, 5),
(109, 6),
(112, 2),
(112, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
`idProducto` mediumint(8) unsigned NOT NULL,
  `nombreProducto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precioProducto` mediumint(9) DEFAULT NULL,
  `stockProducto` mediumint(9) DEFAULT NULL,
  `imagenProducto` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `mostrarProducto` tinyint(1) NOT NULL DEFAULT '1',
  `idCategoria` int(11) NOT NULL,
  `idMarca` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombreProducto`, `precioProducto`, `stockProducto`, `imagenProducto`, `mostrarProducto`, `idCategoria`, `idMarca`) VALUES
(1, 'Metformin HCl', 13, 5, '', 1, 0, 0),
(2, 'Simvastatin', 78, 8, '', 1, 0, 0),
(3, 'Pravastatin Sodium', 87, 8, '', 1, 0, 0),
(4, 'Triamcinolone Acetonide', 97, 5, '', 1, 0, 0),
(7, 'Klor-Con M20', 29, 9, '', 1, 0, 0),
(8, 'Spiriva Handihaler', 93, 3, '', 1, 0, 0),
(9, 'Hydrochlorothiazide', 93, 5, '', 1, 0, 0),
(10, 'Hydrocodone/APAP', 46, 1, '', 1, 0, 0),
(11, 'Gabapentin', 46, 5, '', 1, 0, 0),
(12, 'Promethazine HCl', 35, 2, '', 1, 0, 0),
(13, 'Naproxen', 62, 5, '', 1, 0, 0),
(14, 'Alprazolam', 55, 7, '', 1, 0, 0),
(15, 'Amoxicillin', 10, 3, '', 1, 0, 0),
(16, 'Prednisone', 49, 2, '', 1, 0, 0),
(17, 'Nexium', 75, 6, '', 1, 0, 0),
(18, 'Sertraline HCl', 11, 9, '', 1, 0, 0),
(19, 'Furosemide', 27, 7, '', 1, 0, 0),
(20, 'Alprazolam', 15, 4, '', 1, 0, 0),
(21, 'Warfarin Sodium', 23, 1, '', 1, 0, 0),
(22, 'Lorazepam', 75, 3, '', 1, 0, 0),
(23, 'Vytorin', 75, 4, '', 1, 0, 0),
(24, 'Cialis', 43, 5, '', 1, 0, 0),
(25, 'Celebrex', 57, 2, '', 1, 0, 0),
(26, 'Lisinopril', 79, 5, '', 1, 0, 0),
(27, 'Amphetamine Salts', 51, 2, '', 1, 0, 0),
(28, 'Zolpidem Tartrate', 50, 8, '', 1, 0, 0),
(29, 'Lovaza', 64, 1, '', 1, 0, 0),
(30, 'TriNessa', 49, 1, '', 1, 0, 0),
(31, 'LevothyroxineSodium', 95, 5, '', 1, 0, 0),
(32, 'Lisinopril', 31, 1, '', 1, 0, 0),
(33, 'Klor-Con M20', 58, 9, '', 1, 0, 0),
(34, 'Hydrochlorothiazide', 37, 3, '', 1, 0, 0),
(35, 'Prednisone', 42, 6, '', 1, 0, 0),
(36, 'Tricor', 9, 1, '', 1, 0, 0),
(37, 'Fluticasone Propionate', 83, 3, '', 1, 0, 0),
(38, 'Doxycycline Hyclate', 26, 7, '', 1, 0, 0),
(40, 'Oxycodone/APAP', 82, 2, '', 1, 0, 0),
(41, 'Clindamycin HCl', 41, 4, '', 1, 0, 0),
(42, 'Cheratussin AC', 43, 2, '', 1, 0, 0),
(43, 'Pantoprazole Sodium', 90, 1, '', 1, 0, 0),
(44, 'Prednisone', 72, 4, '', 1, 0, 0),
(45, 'Warfarin Sodium', 39, 3, '', 1, 0, 0),
(46, 'Glipizide', 65, 3, '', 1, 0, 0),
(47, 'Sertraline HCl', 61, 2, '', 1, 0, 0),
(48, 'Benicar HCT', 78, 2, '', 1, 0, 0),
(49, 'Lorazepam', 45, 10, '', 1, 0, 0),
(50, 'Ventolin HFA', 8, 9, '', 1, 0, 0),
(51, 'Oxycontin', 41, 3, '', 1, 0, 0),
(52, 'Triamcinolone Acetonide', 57, 4, '', 1, 0, 0),
(53, 'Oxycodone/APAP', 57, 5, '', 1, 0, 0),
(54, 'Klor-Con M20', 81, 5, '', 1, 0, 0),
(55, 'Vitamin D (Rx)', 78, 6, '', 1, 0, 0),
(56, 'Simvastatin', 42, 2, '', 1, 0, 0),
(57, 'Ventolin HFA', 17, 3, '', 1, 0, 0),
(58, 'Namenda', 64, 9, '', 1, 0, 0),
(59, 'Tramadol HCl', 98, 9, '', 1, 0, 0),
(60, 'Cialis', 41, 2, '', 1, 0, 0),
(61, 'Ciprofloxacin HCl', 95, 1, '', 1, 0, 0),
(62, 'Atenolol', 15, 5, '', 1, 0, 0),
(63, 'Naproxen', 87, 1, '', 1, 0, 0),
(64, 'Carisoprodol', 21, 2, '', 1, 0, 0),
(65, 'Simvastatin', 36, 6, '', 1, 0, 0),
(66, 'Cephalexin', 55, 6, '', 1, 0, 0),
(67, 'Venlafaxine HCl ER', 62, 2, '', 1, 0, 0),
(68, 'Simvastatin', 65, 1, '', 1, 0, 0),
(69, 'Alendronate Sodium', 65, 10, '', 1, 0, 0),
(70, 'Meloxicam', 42, 4, '', 1, 0, 0),
(71, 'Clindamycin HCl', 29, 3, '', 1, 0, 0),
(72, 'Nexium', 98, 5, '', 1, 0, 0),
(73, 'Metformin HCl', 67, 9, '', 1, 0, 0),
(74, 'Lantus', 19, 3, '', 1, 0, 0),
(75, 'Clonazepam', 16, 10, '', 1, 0, 0),
(76, 'Amoxicillin Trihydrate/Potassium Clavulanate', 92, 8, '', 1, 0, 0),
(77, 'Gabapentin', 39, 10, '', 1, 0, 0),
(78, 'Zyprexa', 56, 5, '', 1, 0, 0),
(79, 'Hydrocodone/APAP', 21, 6, '', 1, 0, 0),
(80, 'Lipitor', 52, 5, '', 1, 0, 0),
(81, 'Loestrin 24 Fe', 35, 1, '', 1, 0, 0),
(82, 'Amlodipine Besylate', 28, 6, '', 1, 0, 0),
(83, 'Tri-Sprintec', 43, 10, '', 1, 0, 0),
(84, 'Cyclobenzaprin HCl', 28, 4, '', 1, 0, 0),
(85, 'Doxycycline Hyclate', 81, 10, '', 1, 0, 0),
(86, 'Prednisone', 53, 8, '', 1, 0, 0),
(87, 'Cyclobenzaprin HCl', 81, 1, '', 1, 0, 0),
(88, 'Amlodipine Besylate', 46, 6, '', 1, 0, 0),
(89, 'Nexium', 78, 4, '', 1, 0, 0),
(90, 'Crestor', 44, 3, '', 1, 0, 0),
(91, 'Ciprofloxacin HCl', 24, 2, '', 1, 0, 0),
(92, 'Losartan Potassium', 25, 5, '', 1, 0, 0),
(93, 'Warfarin Sodium', 13, 6, '', 1, 0, 0),
(94, 'Folic Acid', 17, 3, '', 1, 0, 0),
(95, 'Carvedilol', 5, 10, '', 1, 0, 0),
(96, 'Zolpidem Tartrate', 44, 9, '', 1, 0, 0),
(97, 'Paroxetine HCl', 31, 6, '', 1, 0, 0),
(98, 'Hydrocodone/APAP', 17, 8, '', 1, 0, 0),
(99, 'Meloxicam', 10, 3, '', 1, 0, 0),
(100, 'Carvedilol', 71, 1, '', 1, 0, 0),
(102, 'aaaa2', 122, 132, '1479325363_Penguins.jpg', 0, 1, 0),
(105, '2222', 2222, 2222, '1479324737_Hydrangeas.jpg', 1, 2, 0),
(106, '1111111111', 1, 1, '', 1, 1, 1),
(108, '1111', 111, 1111, '', 1, 3, 0),
(109, '1112', 1112, 1112, 'imagen-no-disponible.png', 1, 1, 0),
(110, '111111111', 8388607, 8388607, '', 1, 3, 0),
(111, 'aaaaaaaaaaaaa', 1111, 111111, '', 1, 3, 0),
(112, 'aaaaaaaaaaaaa', 1111, 111111, '', 1, 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`idUsuario` int(11) NOT NULL,
  `nombreUsuario` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `claveUsuario` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `correoUsuario` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaUsuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombreUsuario`, `claveUsuario`, `correoUsuario`, `fechaUsuario`) VALUES
(1, 'david', '5f93e93b937a9af5bf579d5fde6cc347', 'davidfraj@gmail.com', '7441156f553b8ee9bd942f034c2b5ee0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
 ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`idCli`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
 ADD PRIMARY KEY (`idImagen`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
 ADD PRIMARY KEY (`idMarca`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
 ADD PRIMARY KEY (`idNot`);

--
-- Indices de la tabla `posologias`
--
ALTER TABLE `posologias`
 ADD PRIMARY KEY (`idPosologia`);

--
-- Indices de la tabla `posologiasproductos`
--
ALTER TABLE `posologiasproductos`
 ADD PRIMARY KEY (`idPosologia`,`idProducto`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
 ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
MODIFY `idCli` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
MODIFY `idImagen` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
MODIFY `idMarca` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
MODIFY `idNot` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT de la tabla `posologias`
--
ALTER TABLE `posologias`
MODIFY `idPosologia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
MODIFY `idProducto` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
