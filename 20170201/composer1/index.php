<?php  
//Con esto, incluimos las librerias de vendor (composer)
require __DIR__ . '/vendor/autoload.php';

//Cargamos la BBDD
require('includes/config.php');
require('includes/conexion.php');

//Lo primero que hago es establecer el directorio base de mis archivos de plantilla
$loader = new Twig_Loader_Filesystem('views');

//Me creo un objeto de la clase Twig_Environment
$twig = new Twig_Environment($loader, array());
//$twig->getExtension('core')->setDateFormat('d/m/Y','%d dias');
//Estando en mi fichero index, cargo el controlador inicial de la pagina
if(isset($_GET['p'])){
	$p=$_GET['p'];
}else{
	$p='galeria.php';
}

//Llamo al controlador
include('controllers/'.$p);

?>
