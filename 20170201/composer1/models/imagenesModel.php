<?php
require_once('models/imagenModel.php');

Class Imagenes{
	protected $conexion;
	protected $elementos;
	protected $tabla;

	public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
		$this->tabla='imagenes';
	}

	public function dimeImagen($id){
		$sql="SELECT * FROM $this->tabla WHERE idImagen=$id";
		$consulta=$this->conexion->query($sql);
		$fila=$consulta->fetch_array();
		$imagen=new Imagen($fila['idImagen'],$fila['tituloImagenes'], $fila['ficheroImagenes']);
		return $imagen;
	}

	public function dimeImagenes(){
		$sql="SELECT * FROM $this->tabla";
		$consulta=$this->conexion->query($sql);
		while($fila=$consulta->fetch_array()){
			$im=new Imagen($fila['idImagen'],$fila['tituloImagenes'], $fila['ficheroImagenes']);
			//var_dump($im);
			$this->elementos[]=$im;
		}
		return $this->elementos;
	}


}

?>