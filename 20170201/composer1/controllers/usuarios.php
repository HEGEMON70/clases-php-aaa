<?php  
	//Llamamos al modelo
	require_once('models/usuarioModel.php');
	$usuario=new Usuario('Barack', 'Obama');

	//Cargamos los datos
	$datos=array(
		"titulo"=>"Pagina web de usuarios",	
		"subtitulo"=>"Detalle del usuario",
		"usuario"=>$usuario,
		"otra"=>"Otra cosa"
	);

	//Cargo la vista
	//Le digo a mi objeto twig, que plantilla cargar
	$template = $twig->loadTemplate('base.twig.html');

	//Cargo los datos a la vista y renderizo
	echo $template->render($datos);


?>