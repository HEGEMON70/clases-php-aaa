<?php
	//Llamamos al modelo
	require_once('models/imagenesModel.php');
	$imagenes=new Imagenes();

$accion=(isset($_GET['accion']))?$_GET['accion']:'listado';

switch ($accion) {
	case 'listado':
		$titulo='listado Imagenes';
		$subtitulo='Mostramos todas la imagenes';
		//Cargamos los datos
		$datos=array(
		"titulo"=>$titulo,	
		"subtitulo"=>$subtitulo,
		"imagenes"=>$imagenes->dimeImagenes()
		);
		$template = $twig->loadTemplate('galeria.listado.twig.html');
		
		break;

	case 'ver':
		$titulo='detalle de la Imagen';
		$subtitulo='Mostramos una  imagenes';
		$idImagen=$_GET['idImagen'];
		//Cargamos los datos
		$datos=array(
		"titulo"=>$titulo,	
		"subtitulo"=>$subtitulo,
		"imagen"=>$imagenes->dimeImagen($idImagen)
	);
		//Cargo la vista
		//Le digo a mi objeto twig, que plantilla cargar
		$template = $twig->loadTemplate('galeria.ver.twig.html');

		break;
}
	

	

	//Cargo los datos a la vista y renderizo
	echo $template->render($datos);


?>