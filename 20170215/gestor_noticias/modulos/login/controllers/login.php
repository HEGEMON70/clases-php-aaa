<?php 	
//Incluyo mi modelo
require('modulos/login/models/loginModel.php');

//comprobamos si se ha creado la variable de sesion
//$_SESSION['conectado']=isset($_SESSION['conectado'])?$_SESION['conectado']:NULL;
if(!isset($_SESSION['conectado']))
{
	if(isset($_COOKIE['conectado']))
	{
		$_SESSION['conectado']=$_COOKIE['conectado'];
	}
	else
	{
		$_SESSION['conectado']=NULL;
	}
}

//accion de Conectar
if(isset($POST['conectar']))
{
		$Login=new loginModel($_POST['usuario'],$POST['clave']);
		$_SESSION['conectado']=$Login->comprobar(isset($_POST['recordar']));
}
//accion desconectar
if(isset($POST['desconectar']))
{
		$_SESSION['conectado']=NULL;
		setcookie('conectado',NULL,0);	
}
//accion comprobar si estamos conectados o desconectados
if(isset($_SESSION['conectado']))
{
	include ('modulos/login/views/loginView.php');
}
else
{
	include ('modulos/login/views/logoffView.php');
}

//pinto la vista
 

?>