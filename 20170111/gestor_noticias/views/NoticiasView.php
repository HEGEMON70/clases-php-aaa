<?php  
// views/NoticiasView.php
//Parto de que mi coleccion de datos, esta en 
//el vector $elementos
?>
<hr>
<a href="index.php?p=noticias.php&accion=insertar">Alta de noticia</a>
<hr>
<?php foreach ($elementos as $e){ ?>
<article>
	<section>
		<a href="index.php?p=noticias.php&accion=ver&id=<?php echo $e->dimeId();?>">
			<?php echo $e->dimeTitulo();?>
		</a>
		 - 
		<?php echo $e->dimeFecha();?>
		-
		<?php echo $e->dimeAutor();?>
	</section>
	<hr>
</article>
<?php } ?>

<hr>

<?php for ($i=1; $i <= $paginas; $i++) { ?>
	<a href="index.php?p=noticias.php&numpag=<?php echo $i;?>">
		<?php echo $i;?>
	</a>
<?php } ?>
