<?php 
//include 'config.php';
Class Conexion{

	private static $host; //al poner la variable estatica
	private static $usuario;
	private static $clave;
	private static $base;
	private static $conn;

	public static function conectar(){
		self::$host=HOST;
		self::$usuario=USER;
		self::$clave=CLAVE;
		self::$base=BASE;
		
		self::$conn = new mysqli(
									self::$host,
									self::$usuario,
									self::$clave,
									self::$base
									);
		return self::$conn;
	}
	
}
 ?>
