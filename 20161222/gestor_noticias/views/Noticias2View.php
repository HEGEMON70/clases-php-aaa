<?php  
// views/NoticiasView.php
//Parto de que mi coleccion de datos, esta en 
//el vector $elementos
?>
<?php foreach ($elementos as $e){ ?>
<article>
	<header>
		<?php echo $e['tituloNot'];?>
		 - 
		<?php echo $e['fechaNot'];?>
	</header>
	<section>
		<?php echo $e['contenidoNot'];?>
	</section>
	<footer>
		<?php echo $e['autorNot'];?>
	</footer>
</article>
<?php } ?>

<hr>

<?php for ($i=1; $i <= $paginas; $i++) { ?>
	<a href="index.php?p=noticias2.php&numpag=<?php echo $i;?>">
		<?php echo $i;?>
	</a>
<?php } ?>
