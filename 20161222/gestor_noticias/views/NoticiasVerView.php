<?php  
// views/NoticiasVerView.php
?>
<article>
	<header>
		<?php echo $elemento->dimeTitulo();?>
		 - 
		<?php echo $elemento->dimeFecha();?>
		-
		<?php echo $elemento->dimeAutor();?>
	</header>
	<section>
		<?php echo $elemento->dimeContenido();?>	

	</section>
	<hr>
</article>