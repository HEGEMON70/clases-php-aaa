<?php 
include('include/conexion.php');
// if(isset($_GET['p']))//is esta establecido 
// {
// 		$p=$_GET['p'];
// }else{
// 	$p='productos.php';
// }
$p=isset($_GET['p'])?$_GET['p']:'productos.php';
 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<link rel="stylesheet" href="css/estilos.css">
 	<title>Gestor de productos</title>
 </head>
 <body>
 	<!-- section#contenedor>(header#encabezado+nav#menu+section#principal+footer#pie) -->
 	<section id="contenedor">
 		<header id="encabezado">Encabezado</header>
 		<nav id="menu">Menu</nav>
 		<section id="principal">
 		<?php 
 			include('paginas/'.$p) 
 		?>
 		</section>
 		<footer id="pie">Pie</footer>
 	</section>
 </body>
 </body>
 <?php 
$conexion->close();
  ?>
 </html>
