<?php 
// CRUD
// listado (Lista TODOS los productos de la bbdd)
// ver (Muestra informacion detallada del producto)
// insertar (Saca un formulario)
// insercion (Inserta los datos en la bbdd)
// modificar (Saca un formulario con todo rellenado)
// modificacion (Actualiza la bbdd)
// borrar (Borra el registro de la bbdd)

accion();

//////////////////////////////////////////
//COMIENZO DE LA FUNCION accion()
//////////////////////////////////////////
function accion(){
	//Recojo la accion que quiero realizar
	if(isset($_GET['accion'])){
		$accion=$_GET['accion'];
	}else{
		$accion='listado';
	}

	switch($accion){
		case 'ver':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				ver($id);
			}else{
				echo 'error al mostrar el producto';
			}
			break;
		case 'insertar':
			insertar();
			break;
		case 'insercion':
			insercion();
			break;
		case 'borrar':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				borrar($id);
			}else{
				echo 'error al borrar el producto';
			}
			break;
			case 'modificar':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				modificar($id);
			}else{
				echo 'error al modificar el producto';
			}
			break;
			case 'modificacion':
			if(isset($_GET['id'])){
				$id=$_GET['id'];
				modificar($id);
			}else{
				echo 'error al modificar el producto';
			}
			break;
		case 'listado':
		default:
			listado();
			break;
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION accion()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION listado()
//////////////////////////////////////////
function listado(){

	global $conexion;
	global $p;

	if(isset($_GET['orden'])){
		$orden=$_GET['orden'];
	}else{
		$orden='asc';
	}

	if(isset($_GET['campo'])){
		$campo=$_GET['campo'];
	}else{
		$campo='nombreProducto';
	}

	$sql="SELECT * FROM productos ORDER BY $campo $orden";
	$consulta=$conexion->query($sql);

	//Calculo el numero de paginas
	$totalRegistros=$consulta->num_rows;
	$registrosPorPagina=10;
	$numeroDePaginas=ceil($totalRegistros/$registrosPorPagina);

	//Recojo el numero de pagina que quiero ver
	if(isset($_GET['nump'])){
		$nump=$_GET['nump'];
	}else{
		$nump=0;
	}

	$info='Mostrando pÃ¡gina '.($nump+1).' de '.$numeroDePaginas.', ordenado por el '.$campo.', de forma '.$orden;

	?>

	<h2>Gestor de productos - <?php echo $info;?></h2>
	<h3>
		<a href="index.php?p=<?php echo $p; ?>&accion=insertar">
			Alta producto
		</a>
	</h3>
	<hr>
	<table>
		<thead>
			<tr>
				<td>Nombre (<a href="index.php?p=<?php echo $p;?>&orden=asc&campo=nombreProducto">A</a>/<a href="index.php?p=<?php echo $p;?>&orden=desc&campo=nombreProducto">D</a>)</td>
				<td>Precio (<a href="index.php?p=<?php echo $p;?>&orden=asc&campo=precioProducto">A</a>/<a href="index.php?p=<?php echo $p;?>&orden=desc&campo=precioProducto">D</a>) </td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody>
			<?php 
			//Calculo la paginaInicial para la consulta
			$pagInicial=$nump*$registrosPorPagina;

			//Consulta para la paginacion
			$sql="SELECT * FROM productos ORDER BY $campo $orden LIMIT $pagInicial,$registrosPorPagina";
			$consulta=$conexion->query($sql);

			//Recorro los resultados
			while($fila=$consulta->fetch_array()){
				echo '<tr>';
				echo '<td>'.$fila['nombreProducto'].'</td>';
				echo '<td>'.$fila['precioProducto'].'</td>';
				echo '<td><a href="index.php?p='.$p.'&accion=ver&id='.$fila['idProducto'].'">Ver</a>';
				echo '- Editar -';
				echo '<a href="index.php?p='.$p.'&accion=borrar&id='.$fila['idProducto'].'"
				onClick="return confirm(\'¿Estas seguro?\')">
				Borrar</a></td>';
				echo '</tr>';
			}
			?>
		</tbody>
	</table>
	<hr>
	<?php 
	//Que no me deje pulsar en la pagina en la que estoy
	//Que si estoy ordenando por un campo y un orden
	//me mantenga dicho campo y dicho orden
	//Mostrar en algun sitio, el campo y orden
	for ($i=0; $i < $numeroDePaginas; $i++) { 
		if($nump==$i){
			echo ' '.($i+1).' ';
		}else{
			echo ' <a href="index.php?p='.$p.'&nump='.$i.'&orden='.$orden.'&campo='.$campo.'">'.($i+1).'</a> ';
		}
	}

}
//////////////////////////////////////////
//FIN DE LA FUNCION listado()
//////////////////////////////////////////

//////////////////////////////////////////
//COMIENZO DE LA FUNCION ver()
//////////////////////////////////////////
function ver($id){
	global $conexion;
	global $p;

	$sql="SELECT * FROM productos WHERE idProducto=$id";
	$consulta=$conexion->query($sql);
	if($fila=$consulta->fetch_array()){
		echo '<div>';
		echo '<p>'.$fila['nombreProducto'].'</p>';
		echo '<p>'.$fila['precioProducto'].'</p>';
		echo '<p>'.$fila['stockProducto'].'</p>';
		echo '</div>';
	}else{
		echo 'No existe ese producto';
	}

}
//////////////////////////////////////////
//FIN DE LA FUNCION ver()
//////////////////////////////////////////

//////////////////////////////////////////
//COMIENZO DE LA FUNCION insertar()
//////////////////////////////////////////
function insertar(){
	global $conexion;
	global $p;

	?>
		<form action="index.php?p=<?php echo $p;?>&accion=insercion" method="post">
		Nombre:<input type="text" name="nombreProducto"><br>
		Precio:<input type="text" name="precioProducto"><br>
		Stock:<input type="text" name="stockProducto"><br>
		<input type="submit" name="insertar" value="insertar" >
	</form>
	<?php

}
//////////////////////////////////////////
//FIN DE LA FUNCION insertar()
//////////////////////////////////////////

//////////////////////////////////////////
//COMIENZO DE LA FUNCION insercion()
//////////////////////////////////////////
function insercion(){
	global $conexion;
	global $p;
	//recojemos los datos por post
	$nombreProducto=$_POST['nombreProducto'];
	$precioProducto=$_POST['precioProducto'];
	$stockProducto=$_POST['stockProducto'];

	//Genero la consulta sql
	$sql="INSERT INTO productos(nombreProducto,precioProducto,stockProducto)VALUES('$nombreProducto',$precioProducto,$stockProducto)";
	//EJECUTO LA CONSULTA
	if($conexion->query($sql)){
		echo 'Ejecutado con exito';
		header('Location:index.php?p='.$p.'&accion=listado');
	}else{
		echo 'Ha habido un problema';
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION insercion()
//////////////////////////////////////////


//////////////////////////////////////////
//COMIENZO DE LA FUNCION borrar()
//////////////////////////////////////////
function borrar($id){
	global $conexion;
	global $p;

	 $sql="DELETE FROM productos WHERE idProducto=$id";
	if($consulta=$conexion->query($sql)){
		echo 'Ejecutado con exito';
		header('Location:index.php?p='.$p.'&accion=listado');
	}
	else{
		echo 'ha habido un problema';
	}
}
//////////////////////////////////////////
//FIN DE LA FUNCION borrar()
//////////////////////////////////////////
?>

<!-- form>input*3> -->
<!-- COMO DEBUGEAR -->
<!-- <?php 
var_dump($conexion);
?> -->