<?php  
// views/NoticiasVerView.php
?>
<article>
<a href="index.php?p=noticias.php&accion=modificar&id=<?php echo $elemento->dimeId(); ?>">Modificar de noticia</a>
<a href="index.php?p=noticias.php&accion=borrar&id=<?php echo $elemento->dimeId(); ?>">Borrar de noticia</a>
	<header>
		<?php echo $elemento->dimeTitulo();?>
		 - 
		<?php echo $elemento->dimeFecha();?>
		-
		<?php echo $elemento->dimeAutor();?>
	</header>
	<section>
		<?php echo $elemento->dimeContenido();?>	

	</section>
	<hr>
</article>