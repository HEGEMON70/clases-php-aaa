<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class NoticiaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titulo', TextType::class, array('attr' => array('class' => 'form-control', 'placeholder'=>'Titulo')))
            ->add('contenido', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('fecha', DateType::class, array('attr' => array('class' => 'form-control')))
            ->add('autor', TextType::class, array('attr' => array('class' => 'form-control')))

             ->add('idcategoria', EntityType::class, array(
                'class' => 'AppBundle:Categoria',
                'label' => 'Categorias',
                'attr' =>array('class' => 'form-control')))

            ->add('save', SubmitType::class, array('label' => 'Guardar', 'attr'=>array('class'=>'form-control')))
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Noticia'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_noticia';
    }


}
