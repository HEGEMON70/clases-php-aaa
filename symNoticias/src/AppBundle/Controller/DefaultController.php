<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Entity\Noticia;
use AppBundle\Form\NoticiaType;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        //Llamo a el ENTITY MANAGER del controlador
        $em = $this->getDoctrine()->getManager();
        $noticias_rep = $em->getRepository("AppBundle:Noticia");
        
       //Usando nuestro Repositorio de Entidades, extraigo todas
       //$noticias = $noticias_rep->findAll();
        $noticias=$noticias_rep->findBy(array(), array('titulo' => 'ASC'));

        return $this->render('default/index.html.twig', array("noticias"=>$noticias));

        //Para llamar a todos, ordenados de forma DESC por id
        // $noticias=$noticias_rep->findBy(
        //     array(),
        //     array('id' => 'DESC')
        // );

        // $query = $em->createQuery("
        //     SELECT n FROM AppBundle:Noticia n
        //     WHERE n.id > :id
        // ")->setParameter("id","1");

        // $noticias=$query->getResult();

        //$url = $this->generateUrl('homepage_ver',array('id'=>1));

        //return $this->render('default/index.html.twig', array("noticias"=>$noticias, "url"=>$url));
    }

    /**
     * @Route("/ver/{id}", name="homepage_ver")
     */
    public function verAction($id)
    {

        // Creo un ENTITY MANAGER
        $em = $this->getDoctrine()->getManager();
        $noticias_rep = $em->getRepository("AppBundle:Noticia");
        
        $noticia=$noticias_rep->findOneById($id);
        //$noticia=$noticias_rep->findOneByTitulo('bbbb');

       
        // replace this example code with whatever you need
        return $this->render('default/ver.html.twig', array('noticia'=>$noticia));
    }

     /**
     * @Route("/borrar/{id}", name="homepage_borrar")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function borrarAction($id)
    {
        $isAdmin=$view['security']->isGranted('ROLE_ADMIN')?true:false; //ASI SE COMPRUEBA SI ES
        // Creo un ENTITY MANAGER
        $em = $this->getDoctrine()->getManager();
        $noticias_rep = $em->getRepository("AppBundle:Noticia");

        $noticia=$noticias_rep->findOneById($id);

        $em->remove($noticia);
        $flush=$em->flush();
        
        return $this->redirectToRoute('homepage');
        
        // $noticias=$noticias_rep->findBy(array(), array('titulo' => 'ASC'));

        // return $this->render('default/index.html.twig', array("noticias"=>$noticias));
    }

    /**
     * @Route("/nueva", name="homepage_nueva")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function nuevaAction(Request $request)
    {

        //Me creo un objeto de la class de la entidad que quiero agregar
        $noticia=new Noticia();
        $noticia->setFecha(new \DateTime('today'));

        // $form = $this->createFormBuilder($noticia)
        //     ->add('titulo', TextType::class, array('attr' => array('class' => 'form-control', 'placeholder'=>'Titulo')))
        //     ->add('contenido', TextareaType::class, array('attr' => array('class' => 'form-control')))
        //     ->add('fecha', DateType::class, array('attr' => array('class' => 'form-control')))
        //     ->add('autor', TextType::class, array('attr' => array('class' => 'form-control')))
        //     ->add('idcategoria', ChoiceType::class, array('attr' => array('class' => 'form-control')))
        //     ->add('save', SubmitType::class, array('label' => 'Crear Noticia', 'attr'=>array('class'=>'form-control')))
        //     ->getForm();


        $form=$this->createForm(NoticiaType::class, $noticia);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $noticia=$form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($noticia);
            $em->flush();

            return $this->redirectToRoute('homepage');

        }

        return $this->render('default/nueva.html.twig', array(
            'form' => $form->createView()));
    }


    /**
     * @Route("/modificar/{id}", name="homepage_modificar")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function modificarAction(Request $request, $id)
    {
            $em = $this->getDoctrine()->getManager();
            $noticias_rep = $em->getRepository("AppBundle:Noticia");
            $noticia=$noticias_rep->findOneById($id);

            // $form = $this->createFormBuilder($noticia)
            // ->add('titulo', TextType::class)
            // ->add('contenido', TextareaType::class)
            // ->add('fecha', DateType::class)
            // ->add('autor', TextType::class)
            // ->add('save', SubmitType::class, array('label' => 'Modificar Noticia'))
            // ->getForm();

             $form=$this->createForm(NoticiaType::class, $noticia);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $noticia=$form->getData();
                $em->persist($noticia);
                $em->flush();
                return $this->redirectToRoute('homepage');
            }

            return $this->render('default/modificar.html.twig', array(
            'form' => $form->createView()));
    }

}
