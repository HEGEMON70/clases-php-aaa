<?php
//src/AppBundle/Controller/LogiController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;

class LoginController extends Controller
{

    		 /**
		     * @Route("/login", name="homepage_login")
		     */
		    public function loginAction(Request $request)
		    {
		        $authenticationUtils=$this->get("security.authentication_utils");
		        $error=$authenticationUtils->getLastAuthenticationError();
		        $lastUsername=$authenticationUtils->getLastUsername();
		        return $this->render("login/login.html.twig", array("error"=>$error, "last_username"=>$lastUsername));
		        //ref: http://symfony.com/doc/current/security/form_login_setup.html
		        //usu: hegemon70@gmail.com  pwd: 1234
		    }

		    /**
		     * @Route("/login_check", name="login_check")
		     */
		    public function login_checkAction(Request $request)
		    {

		    }

		    /**
		     * @Route("/logout", name="logout")
		     */
		    public function logoutAction(Request $request)
		    {

		    }
		    /**
		     * @Route("/registro", name="login_registro")
		     */
		    public function registroAction(Request $request)
		    {
		    	//TODO  POR TERMINAR
		    	//TODO  POR TERMINAR);
				$form = $this->createForm(UsuarioType::class,$user);
				
				$form->handleRequest($request);
				if($form->isSubmitted()){
					if($form->isValid()){
						$em=$this->getDoctrine()->getEntityManager();
						$user_repo=$em->getRepository("AppBundle:User");
						$user = $user_repo->findOneBy(array("email"=>$form->get("email")->getData()));
						
						if(count($user)==0){
							$user = new User();
							$user->setName($form->get("name")->getData());
							$user->setSurname($form->get("surname")->getData());
							$user->setEmail($form->get("email")->getData());

							$factory = $this->get("security.encoder_factory");
							$encoder = $factory->getEncoder($user);
							$password = $encoder->encodePassword($form->get("password")->getData(), $user->getSalt());

							$user->setPassword($password);
							$user->setRole("ROLE_USER");
							$user->setImagen(null);

							$em = $this->getDoctrine()->getEntityManager();
							$em->persist($user);
							$flush = $em->flush();
							if($flush==null){
								$status = "El usuario se ha creado correctamente";
							}else{
								$status = "No te has registrado correctamente";
							}
						}else{
							$status = "El usuario ya existe!!!";
						}
					}else{
						$status = "No te has registrado correctamente";
					}

					$this->session->getFlashBag()->add("status",$status);
				}
				return $this->render("AppBundle:User:login.html.twig", array(
					"error" => $error,
					"last_username" => $lastUsername,
					"form" => $form->createView()
				));
				//TODO  POR TERMINAR
				//TODO  POR TERMINAR
		    }


}
