DROP DATABASE IF EXISTS `proyecto01`;


-- -----------------------------------------------------
--   schema proyecto01
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `proyecto01` DEFAULT CHARACTER SET utf8 ;
USE `proyecto01` ;

-- -----------------------------------------------------
-- Table `proyecto01`.`formulas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proyecto01`.`producto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200),
  `precio` FLOAT NULL,
   `unidades` INT NULL,
  `imagen` VARCHAR(200) NULL,
  `alta` DATETIME  NULL,

  PRIMARY KEY (`id`))
;