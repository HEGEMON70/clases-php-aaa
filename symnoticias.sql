-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-03-2017 a las 18:50:04
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `symnoticias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);
--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Diseño', 'Categoria sobre diseño'),
(2, 'Programacion', 'Categoria sobre programacion'),
(3, 'Politica local','Categoria sobre politica local');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id`, `titulo`, `contenido`, `autor`, `fecha`, `id_categoria`) VALUES
(1, 'aaaa', 'aaaaaa', 'ddddd', '2012-01-01', 2),
(4, 'Titulo de la noticia', 'Contenido de la noticia', 'David', '2012-12-01', 1),
(5, 'Hoy es martes2', 'Contenido de hoy es martes2', 'David2', '2017-02-14', 1);
(2,'podemos vence', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit eos, omnis quaerat iste provident molestias nemo dicta perspiciatis earum enim ipsa sint minima tempora repellendus, itaque temporibus nisi praesentium, veritatis.', 'fernando berlin', '2017-02-27',1),
(3,'podemos pierde', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur numquam consectetur doloremque libero recusandae eaque eveniet provident sunt magnam optio qui laudantium nisi expedita aliquid illum cum, soluta quibusdam nemo.', 'federico lossantos', '2017-02-21',1),
(6,'podemos la caga', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit eos, omnis quaerat iste provident molestias nemo dicta perspiciatis earum enim ipsa sint minima tempora repellendus, itaque temporibus nisi praesentium, veritatis.', 'pilar gomez borrero', '2017-02-27',2),
(7'podemos nifu nifa', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit eos, omnis quaerat iste provident molestias nemo dicta perspiciatis earum enim ipsa sint minima tempora repellendus, itaque temporibus nisi praesentium, veritatis.', 'iñaki Gabilondo', '2017-02-27',2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--



--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `noticia`
--
ALTER TABLE `noticia`
  ADD CONSTRAINT `noticia_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE usuario(
    id       int(255) auto_increment not null,
    role     varchar(20),
    nombre     varchar(255),
    apellidos  varchar(255),
    email    varchar(255),
    password varchar(255),
    CONSTRAINT pk_usuario PRIMARY KEY(id)
);

INSERT INTO `usuario` ( id, role, nombre, apellidos, email, password ) VALUES (1, 'ROLE_ADMIN' ,'fernando' ,'mendiz' ,'HEGEMON70@GMAIL.COM' ,'$2a$04$NtM.aexhjAyIxjR0cKQ8V.4NJIWzIJnG44.VBq4hAoSAOZ2qH8a9u');