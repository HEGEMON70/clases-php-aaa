<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Producto;
class ProductosController extends Controller
{
    /**
     * @Route("/productos", name="productos_index")
     */
    public function indexAction(Request $request)
    {
       echo 'Listado de productos';
       die();
    }

    /**
     * @Route("/productos/alta", name="productos_alta")
     */
    public function altaAction(Request $request)
    {
       echo 'Alta de productos';
        //sin usar use arriba
       //$p=new AppBundle\Entity\Producto();
       $p = new Producto();
       $p->setNombre('impresora');
       $p->setPrecio(125);
       $p->setUnidades(4);
       $p->setImagen(125);
       $p->setAlta(125);
      

    
        //obtenenmos un manejador que esta en controller
       $em = $this->getDoctrine()->getManager();
       //sobre el manejador PERSISTIMOS, el nuevo producto (un commit trnasaction)
       $em->persist($p);

       $flush=$em->flush();
       die();
    }

  
}
