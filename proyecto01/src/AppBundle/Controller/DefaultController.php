<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/holamundo", name="holamundo")
     */
    public function holaMundo()
    {
        echo "Hola mundo";

        die();
    }

    /**
     * @Route("/holamundo2/{numero}", name="holamundo2")
     */
    public function holaMundo2($numero=1)
    {

        return $this->render('default/holamundo.html.twig', array("numero"=>$numero));

    }

    public function holaMundo3Action($numero=1)
    {
        return $this->render('default/holamundo.html.twig', array("numero"=>$numero));

    }

}
