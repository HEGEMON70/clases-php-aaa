<?php 
//Archivo en el directorio bbdd
//Nombre: conexion1.php


//PHP tiene una Class llamada Mysqli

//php 5.3 5.4


// $conexion=mysqli_connect('localhost','user','123345','empresa');
// mysqli_query($conexion,$sql);


//1- conectar a la base de datos
$host='localhost';//@ip del servidor
$usuario='root';//el usuario de xammpp
$clave='';//pasw vacia
$base='empresa';
//con funciones
$conexion=mysqli_connect($host,$usuario,$clave,$base);
mysqli_set_charset($conexion,'UTF8');
//con Objetos
$conexion2= new Mysqli($host,$usuario,$clave,$base);
$conexion2->set_charset('UTF8');
//2- establecer la consulta  pregunta
$sql="SELECT * FROM productos";//en mayusculas para distinguir si son de sql
//3- ejecuto la pregunta
//con funcion
$consulta=mysqli_query($conexion,$sql);//en consulta un FALSE si falla, si no falla,  Si es de insercion false si falla true si acierta
//con objeto
$consulta2=$conexion2->query($sql);
//4- si select muestro los datos, si es de insercion comprobar
//con funciones
while($fila=mysqli_fetch_array($consulta)){
	echo $fila['nombreProducto'];
	// echo $fila['precioProducto'];
	echo '<br>';
}

//con objetos

while($fila=$consulta2->fetch_array()){
	echo $fila['nombreProducto'];
	echo '<br>';
}
//5- desconexion de la BBDD
//con funciones
mysqli_close($conexion);
//con objetos
$conexion2->close();

 ?>