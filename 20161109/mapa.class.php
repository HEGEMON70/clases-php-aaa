<?php 
//Clase para crear mapas
class Mapa extends Imagen{

//creo propiedades del mapa
	public $nombre;
	public $areas; //vector con las areas del mapa
	//metodos de la clase hija

	public function __construct($archivo,$titulo,$nombre)
	{
		parent::__construct($archivo,$titulo);///llamo al padre
		$this->nombre=$nombre;
		$this->areas = array();
	}

	public function dibujar()
	{
		$r=substr(parent::dibujar(),0,strlen(parent::dibujar())-5); //quita los ultimo 5 char del codigo del padre
		$r.='usemap?"#"'.$this->nombre.'"><br>'; //añade 
		$r.='<map name="'.$this->nombre.'">';  
		foreach($areas as $area){
			$r.=$area->dibujar();
		}

		$r.='</map>';
		return $r;
	}

	public function getAncho()
	{
		return $this->ancho;
	}

	public function addArea($archivo,$titulo,$enlace)
	{
		$area=new Area($archivo,$titulo,$enlace);
		$this->areas[]=$area;//añadimos unnuevo objeto de la clase area
	}
}
 ?>
