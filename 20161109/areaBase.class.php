<?php 

class AreaBase{
//EJEMPLO DE POLIMORFISMO
	//public $tipo;
	public $coordenadas;
	public $enlace;
	public static $contador=0;


//metodo statico
	public static  function saluda(){
		echo 'Hola';
	}
	//metodos
	public function __construct($coordenadas,$enlace)
	{
		//$this->tipo=$tipo;
		$this->coordenadas=$coordenadas;
		$this->enlace=$enlace;
		self::$contador++;//incremento  la variable estatica de contador
	}
}//fin area base

	//class Rect
	class Rect extends AreaBase{
			public function dibujar()
			{
				return 'area type="rect" coords="'.$this->coordenadas.'" href="'.$this->enlace.'">';
			}
	}//fin de class Rect

	class Circ extends AreaBase{
		public function dibujar()
		{
			return 'area type="circ" coords="'.$this->coordenadas.'" href="'.$this->enlace.'">';
		}
	}//fin de class Circ

	class Poly extends AreaBase{
		public function dibujar()
		{
			return 'area type="poly" coords="'.$this->coordenadas.'" href="'.$this->enlace.'">';
		}		
	}//fin de class Poly



$areas=array(new Circ('23,34,10','web1.html'),new Rect('23,34,67,129','web2.html'));
						for ($i=0; $i < count($areas) ; $i++) {
								echo $areas[$i]->dibujar();
							}//este bucle detectara de cada elem el tipo y dibujara segun su tipo 



echo AreaBase::$contador;
echo AreaBase::saluda();


echo '---------------------------<br>';
$a= new AreaBase('12,23','web1.php');
$b=$a;
echo $a->coordenadas;
echo '<br>'; 
echo $a->coordenadas;
echo'<br>';
$b->coordenadas='20,30';
echo $a->coordenadas;
echo '<br>'; 
echo $a->coordenadas;
echo'<br>';
///cambiamos los dos porque es el mismo objeto
echo '---------------------------<br>';
$a1= new AreaBase('12,23','web1.php');
$b= clone $a1;//clona el objeto completo
echo $a1->coordenadas;
echo '<br>'; 
echo $a1->coordenadas;
echo'<br>';
$b->coordenadas='20,30';
echo $a1->coordenadas;
echo '<br>'; 
echo $a1->coordenadas;
echo'<br>';
//si quiero



 ?>
