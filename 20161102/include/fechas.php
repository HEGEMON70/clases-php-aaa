<?php 
//Funcion time()
echo time(); //devuelve la fecha timestamp actual
echo '<br>';

//Funcion date()
$meses=array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

$dias=array("Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado");

echo date("d/m/Y H:i:s F", time());
echo '<br>';
echo $meses[date("m")];
echo '<br>';

//Queremos hacerlo de tal manera, que nos muestre lo siguiente:
//Hoy es Miercoles, 26 de Octubre de 2016

echo 'Hoy es '.$dias[date('w')].', '.date('d').' de '.$meses[date("n")].' de '.date('Y');

?>

<!-- select>option[value="$"]{opcion $}*12 -->


<form action="fechas.php" method="post">
<?php 
	echo '<select name="dia">';
	for ($i=1; $i <= 31 ; $i++) { 
		if($i==date('j')){
			echo '<option value="'.$i.'" selected>'.$i.'</option>';
		}else{
			echo '<option value="'.$i.'">'.$i.'</option>';		
		}
	}	
	echo '</select>';

	echo '<select name="mes">';
	for ($i=1; $i <= 12 ; $i++) { 
		if($i==date('n')){
			echo '<option value="'.$i.'" selected>'.$meses[$i].'</option>';
		}else{
			echo '<option value="'.$i.'">'.$meses[$i].'</option>';		
		}
	}	
	echo '</select>';

	echo '<select name="anyo">';
	for ($i=(date('Y')-10); $i <= (date('Y')+10) ; $i++) { 
		if($i==date('Y')){
			echo '<option value="'.$i.'" selected>'.$i.'</option>';
		}else{
			echo '<option value="'.$i.'">'.$i.'</option>';		
		}
	}	
	echo '</select>';
?>
<input type="submit" value="enviar" name="enviar">
</form>

<?php 
//Miro a ver si muestro la fecha elegida
if(isset($_POST["enviar"])){

	$dia=$_POST['dia'];
	$mes=$_POST['mes'];
	$anyo=$_POST['anyo'];

	echo "$dia/$mes/$anyo";
	echo '<br>';
	//Genero la fecha timestamp, con la info que tenemos
	$hora=0;
	$minuto=0;
	$segundos=0;
	$f=mktime($hora, $minuto, $segundos, $mes, $dia, $anyo);

	echo 'Has elegido el '.$dias[date('w', $f)].', '.date('d', $f).' de '.$meses[date("n", $f)].' de '.date('Y', $f);


}

?>