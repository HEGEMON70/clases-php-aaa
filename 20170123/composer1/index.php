<?php 
require __DIR__ . '/vendor/autoload.php'; //enlace de composer

//establezco el directorio base de mis archivos de plantilla
$loader = new Twig_Loader_Filesystem('plantillas');
//Me creo el objeto de la clase Twig_Enviroment
$twig = new Twig_Environment($loader, array());//el segundo argumento es un array vacio(para parametros)

//le digo a mi objeto twig, que plantillas cargar
$template = $twig->loadTemplate('base.twig.html');

Class Usuario{

	public $nombre;
	public $apellidos;
	public function __construct($n,$a){
			$this->nombre=$n;
			$this->apellidos=$a;
	}
}
$usuario= new Usuario('Barak', 'Obama');

 $datos =array(
 	"titulo"=>"Hola mundo",
 	"subtitulo"=>"que tal estas",
 	"usuario"=>$usuario
 	);
//echo $template->render(array("titulo"=>"Hola mundo"));
echo $template->render($datos); 
 ?>

