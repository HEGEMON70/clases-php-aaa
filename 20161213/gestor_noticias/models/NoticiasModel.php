<?php  
//Incluyo mi modelo de noticia individual
include('models/NoticiaModel.php');

//Modelo de noticias
Class NoticiasModel{
	private $conexion;
	private $elementos;
	private $elementosPorPagina;

	public function __construct(){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
		$this->elementosPorPagina=5;
	}

	public function listado($numpag){
		$pagInicial=($numpag-1)*$this->elementosPorPagina;
		$sql="SELECT * FROM noticias LIMIT  $pagInicial, $this->elementosPorPagina";
		$consulta=$this->conexion->query($sql);



		while($fila=$consulta->fetch_array()){
			$not=new NoticiaModel($fila['idNot'], 
								  $fila['tituloNot'], 
								  $fila['contenidoNot'], 
								  $fila['autorNot'], 
								  $fila['fechaNot']);
			$this->elementos[]=$not;
		}
		return $this->elementos;
	}

	public function elementosPorPagina($numElementos){
		if($numElementos>0){
		$this->$elementosPorPagina=$numElementos;
		}
	}
	public function dimePaginas(){
		$sql="SELECT * FROM noticias";
		$consulta=$this->conexion->query($sql);
		//sacamos registro
		$totalRegistros=$consulta->num_rows;
		//dividimos para el numero de elem por pagina y lo redondeamos
		return ceil($totalRegistros/$this->elementosPorPagina); 
	}



}


?>