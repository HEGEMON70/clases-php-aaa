<?php  
//Archivo que llamara al modelo de datos, y cargara la vista
// controllers/noticias.php

include('models/NoticiasModel.php');
$modelo=new NoticiasModel();
$modelo->elementosPorPagina(10); //opcional
//quiero recoger el numero de paginas por GET
//sera la pagina actual
$numpag=isset($_GET['numpag'])?$_GET['numpag']:1;//
$elementos=$modelo->listado($numpag);//Saco las noticia
$paginas=$modelo->dimePaginas;//saco cuantas paginas tengo

include('views/NoticiasView.php');

?>