Lunes 16 de Enero de 2017
--------------------------

Crear una cesta de la compra
Crear las urls amigables de los productos

index.php
conexion.php
carrito.php
listadoProductos.php
detalleProducto.php

productos
---------
idPro
nombrePro
precioPro

--------------------------------------------

TAREA 1:
	Añadir la posibilidad de tener UNIDADES
	
	Si hacemos click en la cesta desde el listado, se añade una unidad, a no ser que el producto YA este en la cesta, así que se sumara 1 elemento al dicho numero de unidades

	Si o hacemos desde el detalle del producto, queremos poder elegir el número de unidades, ANTES de añadir el producto al carrito.

	NOTA: Si ya teniamos 2 unidades, y añadimos otras 3, deberán aparecer 5 unidades.

--------------------------------

URL amigables.

	A los buscadores, como google, NO les gustan las direcciones del tipo:

		http://localhost:8080/aaa/20170116_lunes/index.php?p=detalleProducto.php&idPro=2

		http://www.aragondigital.es/noticia.asp?notid=152185&secid=17

		http://www.heraldo.es/noticias/deportes/futbol/real-zaragoza/2017/01/15/el-real-zaragoza-jugara-champions-los-videojuegos-1153339-611027.html

	-> Se puede usar un módulo de APACHE, llamado MOD_REWRITE
	-> Este modulo AFECTA a la configuración de TODO el servidor

	-> Reescritura de sitios, usando un archivo llamado .htaccess