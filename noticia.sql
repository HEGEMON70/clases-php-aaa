-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-02-2017 a las 19:30:41
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `symnoticias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE `noticia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
	`id_categoria` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_noticias_categoria`
    FOREIGN KEY (`categoria_id`)
    -- REFERENCES `farmacia`.`personas` (`idpersonas`)
    REFERENCES `symnoticias`.`categorias` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` ( `titulo`, `contenido`, `autor`, `fecha`,`categoria_id`) VALUES
('podemos vence', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit eos, omnis quaerat iste provident molestias nemo dicta perspiciatis earum enim ipsa sint minima tempora repellendus, itaque temporibus nisi praesentium, veritatis.', 'fernando berlin', '2017-02-27',1),
('podemos pierde', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur numquam consectetur doloremque libero recusandae eaque eveniet provident sunt magnam optio qui laudantium nisi expedita aliquid illum cum, soluta quibusdam nemo.', 'federico lossantos', '2017-02-21',1),
('podemos la caga', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit eos, omnis quaerat iste provident molestias nemo dicta perspiciatis earum enim ipsa sint minima tempora repellendus, itaque temporibus nisi praesentium, veritatis.', 'pilar gomez borrero', '2017-02-27',2),
('podemos nifu nifa', ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit eos, omnis quaerat iste provident molestias nemo dicta perspiciatis earum enim ipsa sint minima tempora repellendus, itaque temporibus nisi praesentium, veritatis.', 'iñaki Gabilondo', '2017-02-27',2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticia`
--

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticia`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
