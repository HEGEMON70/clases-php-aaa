<?php  
Class MasterModel{
	protected $conexion;
	protected $elementos;
	protected $elementosPorPagina;
	protected $ordenColumna;
	protected $ordenV;
	protected $tabla;

	public function __construct($tabla){
		$this->conexion=Conexion::conectar();
		$this->elementos=array();
		$this->elementosPorPagina=NEPP;
		$this->ordenV='ASC';

		$sql="SELECT * FROM $tabla LIMIT 0,1";
		$consulta=$this->conexion->query($sql);
		$this->ordenColumna = $consulta->fetch_fields()[0]->name;//comprobar el metodo mysqli_result::fetch_fields
		$this->tabla=$tabla;

	}
	
	public function elementosPorPagina($numElementos=NEPP){
		if($numElementos>0){
			$this->elementosPorPagina=$numElementos;
		}
	}
	public function dimePaginas(){
		$sql="SELECT * FROM $this->tabla";
		$consulta=$this->conexion->query($sql);
		$total=$consulta->num_rows;
		//los dividimos para el numero de elementos por pagina
		//Lo redondeamos ceil(numero)
		//Lo devolvemos
		return ceil($total/$this->elementosPorPagina);
	}
	public function estableceOrden($ordenC,$ordenV)
	{
		$this->ordenColumna=$ordenC;
		$this->ordenV=$ordenV;

	}
}
?>