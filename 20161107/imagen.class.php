<?php  

//Creamos una clase llamada tabla
//Fecha: 
//historico: 

///final class Imagen // implica que mo permite herencia
class Imagen{
	public $archivo;
	public $titulo;
	public $borde;
	protected $ancho;//nivel de proteccion para que sea accesible desde las calses que heredan

	public function __construct($archivo, $titulo){
		$this->archivo=$archivo;
		$this->titulo=$titulo;
		$this->borde=0;
		$this->ancho=400;
	}

	public function dibujar(){
		return '<img src="'.$this->archivo.'" title="'.$this->titulo.'" border="'.$this->borde.'" width="'.$this->ancho.'"><br>';
	}

	public function setAncho($ancho){
		if(is_numeric($ancho)){
			$this->ancho=$ancho;
		}else{
			echo "Tienes que introducir un numero";
		}
	}

	public function dimeTitulo(){
		return '<caption>'.$this->titulo.'</caption>';
	}
}

?>