<?php 

class Imagen{

public $archivo;
public $titulo;
public $borde;
public $ancho;

	public function __construct($archivo,$titulo){
		$this->archivo=$archivo;
		$this->titulo=$titulo;
		$this->borde=0;
		$this->ancho=400;

	}
	public function dibujar(){
		return '<img src="'.$this->archivo.'" title="'.$this->titulo.'" border="'.$this->borde.'" width="'.$this->ancho.'"><br>';
	}
	public function dimeTitulo(){
		return '<caption>'.$this->titulo-'</caption>';
	}

	public function setAncho($ancho){
		if(is_numerid($ancho)){
			$this->ancho=$ancho;
		}
		else
		{
			echo "Tienes que introducir un numero";
		}
	}

}

$img1 = new Imagen('medusa.jpg','Medusa en el fondo del Mar');
$img2 = new Imagen('koala.jpg','Koala en el arbol');
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 </head>
 <body>
 	<?php  

 		echo $img1->dibujar(); 
 		echo $img1->dimeTitulo();

 		echo $img2->dibujar(); 
 		echo $img2->dimeTitulo();
 	?>

 </body>
 </html>
